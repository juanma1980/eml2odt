#!/usr/bin/env python3
#Copyright valsoft juanma1980@disroot.org
#This code is licensed under GPL3
#This script uses emailconverter to convert eml files to odt
import os
import shutil
import sys
import subprocess
import logging
import urllib.request

DBG=True
EMAILCONVERTERURL="https://github.com/nickrussler/email-to-pdf-converter/releases/download/2.5.3/emailconverter-2.5.3-all.jar"
JARPATH=os.path.join("/tmp",os.path.basename(EMAILCONVERTERURL))
JAR="java -jar {} -hh -a".format(JARPATH)

def _debug(msg):
	if self.dbg:
		logging.warning("rebost: %s"%str(msg))
#def _debug

def downloadEmailconverter():
	if os.path.isfile(JARPATH)==False:
		urllib.request.urlretrieve(EMAILCONVERTERURL,JARPATH)
#def downloadEmailconverter

def convertEmlFile(eml):
	cmd=JAR.split()
	cmd.append(eml)
	try:
		subprocess.run(cmd)
	except Exception as e:
		_debug(e)
#def convertEmlFile

def processDir():
	dirFiles=os.listdir(".")
	for f in dirFiles:
		fixedname=f.replace(" ","_")
		if fixedname.endswith(".eml"):
			shutil.move(f,fixedname)
			convertEmlFile(fixedname)
#def processDir


downloadEmailconverter()
processDir()
